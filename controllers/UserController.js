const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')


module.exports.checkIfEmailExists = (data)  => {
	//({email: data.email}) data caller function that will be put in postman
	return User.find({email: data.email}).then((result) => {
		// anything greater than 0 means duplicate email found
		
		if (result.length > 0){ 
			return true
		}
		return false
	})
}


module.exports.register = (data) => { 

// 'bcrypt.hashSync()' method to assign encryption for sensitive datas
let encrypted_password = bcrypt.hashSync(data.password, 10)

// whenever a new data is added always declare a new variable to input the new data needed referencing the models
let new_user = new User({
	firstName: data.firstName,
	lastName: data.lastName,
	email: data.email,
	mobileNo: data.mobileNo,
	password: encrypted_password
})
// .save method to save the data onto the database
	return new_user.save().then((created_user, error) =>{
		if(error){
			return false
		}

		return {
			message: 'User successfully registered!'
		}
	})
}



module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if (result == null){
			return {
				message: "User doesn't exist!"
			}
		}

		//.compareSync returns a boolean value type
		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if (is_password_correct) {
			return {
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}


	})
}



module.exports.getUserDetails = (user_id) => {
		return User.findById(user_id, {password:0})
		.then((result, error) => {
		if (error){
				console.log(error)
				return error
			}

		return result
	})
}

module.exports.enroll = async (data) => {
	
//Check if user is done adding the course to its enrollments array
	let is_user_updated = await User.findById(data.userId).then((user) => {
		user.enrollments.push({
			courseId: data.courseId
		})

		return user.save().then((updated_user, error) => {
			if (error){
				return false
			}

			return true
		})


	})

// Check if course is done adding the user to its enrollees array
	let is_course_updated = await Course.findById(data.courseId).then((course) => {
		course.enrollees.push({
			userId: data.userId
		})

		return course.save().then((updated_course, error) => {
			if (error){
				return false
			}

			return true
		})

	})

	//Check if both user and course have been updated successfully, and return a success message if so
	if(is_user_updated && is_course_updated){
		return {
			message: 'User enrollment is successful!'
		}
	}


	//If the enrollment failed, return 'Something went wrong.'
	return{
		message: 'Something went wrong'
	}
}